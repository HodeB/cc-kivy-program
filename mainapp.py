from os import listdir, getcwd
from sh import zip, python3, mv


def get_extension_length(filename):
    '''
    Returns how long the filetype extension is
    '''
    for i in range(1, len(filename)):
        if filename[-i] == '.':
            return i


def strip_files(files, extension,):
    '''
    Returns a list of files with given extension
    '''
    array = []
    for f in files:
        if f[-get_extension_length(f):] == extension:
            array.append(f)

    return array


def unpack_directory(directory):
    '''
    Returns all the files found in an directory and all its subdirectories
    '''
    files = []

    if directory[-1:] != '/':
        directory += '/'

    for f in listdir(directory):
        try:
            listdir(directory + f)
        except NotADirectoryError:
            files.append(directory + f)
        else:
            for f in unpack_directory(directory + f):
                files.append(f)

    return files


cwd = getcwd() + '/bilder'
files = unpack_directory(cwd)

for f in files:
    python3('kivyapp.py', f)

cwd = getcwd() + '/rendered'
files = unpack_directory(cwd)
files = strip_files(files, '.png')
tozip = []

for f in files:
    if '000' in f:
        name = f.split('/')[-1]
        name = cwd + '/' + name[:-8] + '.png'
        mv(f, name)
        tozip.append(name)

zip('rendered/output.zip', ' '.join(tozip))
