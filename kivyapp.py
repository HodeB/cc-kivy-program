from kivy.app import App
from kivy.core.window import Window
from kivy.clock import Clock
from kivy.graphics import Color, Rectangle
from kivy.uix.image import Image
from kivy.uix.widget import Widget
from os import listdir, getcwd
import sys


class Sprite(Image):
    def __init__(self, logo, name, **kwargs):
        super(Sprite, self).__init__(**kwargs)
        self.size = self.texture_size

        if logo:

            self.filename = name
            self.name = name.split('/')[-1]


class CCWidget(Widget):
    def __init__(self, **kwargs):
        super(CCWidget, self).__init__(**kwargs)
        self.logo = None

        cwd = getcwd() + '/bilder'
        files = unpack_directory(cwd)

        bannername = getcwd() + '/logo/ccbanner.png'
        self.banner = Sprite(False, 'ccbanner.png', source=bannername)

        try:
            logoname = sys.argv[1]
        except: # bad habit, if we know what exception is gonna be raised we should use that.
            print('Please specify logo')
            exit()

        for f in strip_files(files, '.png'):
            if logoname in f:
                self.logo = Sprite(True, f, source=f)

        with self.canvas:
            Color(1, 1, 1)
            Rectangle(pos=(0, 0), size=self.banner.size)

        self.add_widget(self.banner)
        Window.size = self.banner.size

        self.i = 0

    def update(self, dt):

        if self.i == 0:
            logo = self.logo

            targetheight = self.banner.height / 1.9  # Resize logo
            targetwidth = self.banner.width / 1.1
            logo.keep_ratio = True
            logo.height = targetheight
            if logo.width > targetwidth:
                logo.width = targetwidth

            logo.x = (self.banner.width / 2 - logo.width / 2)
            logo.y = (self.banner.height / 2 -
                      logo.height / 2 -
                      self.banner.height / 7.1)

            self.add_widget(logo)

        if self.i == 2:
            name = 'rendered/' + self.logo.name
            Window.screenshot(name)
        if self.i == 6:
            exit()

        self.i += 1

class CCApp(App):
    def build(self):
        widget = CCWidget()
        widget.update('dt')
        Clock.schedule_interval(widget.update, 0.1)
        return widget


def get_extension_length(filename):
    '''
    Returns how long the filetype extension is
    '''
    for i in range(1, len(filename)):
        if filename[-i] == '.':
            return i


def strip_files(files, extension,):
    '''
    Returns a list of files with given extension
    '''
    array = []
    for f in files:
        if f[-get_extension_length(f):] == extension:
            array.append(f)

    return array


def unpack_directory(directory):
    '''
    Returns all the files found in an directory and all its subdirectories
    '''
    files = []

    if directory[-1:] != '/':
        directory += '/'

    for f in listdir(directory):
        try:
            listdir(directory + f)
        except NotADirectoryError:  # NOQA
            files.append(directory + f)
        else:
            for f in unpack_directory(directory + f):
                files.append(f)

    return files

if __name__ == '__main__':
    CCApp().run()
