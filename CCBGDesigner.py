from easygui import fileopenbox, filesavebox
from os import getcwd
from PIL import Image

logo = fileopenbox()
banner = Image.open(getcwd() + '/logo/ccbanner.png')
img = Image.open(logo)
width = int(banner.width / 2)
height = int(banner.height / 2) + 70

while img.width > banner.width * 0.9:
    img = img.resize((int(img.width / 1.1),
                      int(img.height / 1.1)),
                     Image.ANTIALIAS)

while img.height > banner.height * 0.53:
    img = img.resize((int(img.width / 1.1),
                      int(img.height / 1.1)),
                     Image.ANTIALIAS)

banner.paste(img, (width - int(img.width / 2),
                   (height - int(img.height / 2))),
             img)

banner.save(filesavebox())
